package models

// MessageRequest ..
type MessageRequest struct {
	Email       string `json:"email"`
	Subject     string `json:"subject"`
	Message     string `json:"message"`
	CompanyName string `json:"companyName"`
	Phonenumber string `json:"phonenumber"`
	Country     string `json:"country"`
}

type MessageAuthRequest struct {
	UserList []string `json:"userList"`
}
type MessageDataAuth struct {
	Username     string `json:"username"`
	Site         string `json:"site"`
	Email        string `json:"email"`
	SlackHook    string `json:"slackHook"`
	Token        string `json:"token"`
	ExpiredToken int64  `json:"expiredToken"`
}

// MessageResponse ..
type MessageResponse struct {
	Rc      string      `json:"rc"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}
type DBMessageDataAuth struct {
	Username      string `json:"username" gorm:"column:username"`
	Site          string `json:"site" gorm:"column:site"`
	Email         string `json:"email" gorm:"column:email"`
	EmailPassword string `json:"emailPassword" gorm:"column:email_password"`
	SlackHook     string `json:"slackHook" gorm:"column:slack_hook"`
	Token         string `json:"token" gorm:"column:token"`
	ExpiredToken  int64  `json:"expiredToken" gorm:"column:expired_token"`
}

func (t *DBMessageDataAuth) TableName() string {
	return "mail_user"
}
