package utils

import (
	"math/rand"
	"os"
	"strconv"
	"time"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func GetEnvString(key string, defaultResponse string) string {
	str := os.Getenv(key)
	if str == "" {
		str = defaultResponse
	}
	return str
}

func GetEnvInt(key string, defaultResponse int) int {
	str := os.Getenv(key)
	if str == "" {
		res := defaultResponse
		return res
	}
	res, _ := strconv.Atoi(str)
	return res
}

// Rand64String is for generate random string 64
func Rand64String(n int) string {
	//todo bisa di pindahkan di global variabel
	var src = rand.NewSource(time.Now().UnixNano())

	b := make([]byte, n)
	l := len(letterBytes)
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < l {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
