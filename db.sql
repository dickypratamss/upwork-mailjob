create database upwork;

create table mail_user( 
    id int auto_increment primary key, 
    username varchar(255) unique, 
    site varchar(255), 
    email varchar(100), 
    email_password varchar(100), 
    slack_hook varchar(255), 
    token varchar(255), 
    expired_token int);