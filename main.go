package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/smtp"
	"strings"
	"time"
	"upwork-mailjob/db"
	"upwork-mailjob/models"
	"upwork-mailjob/utils"

	"github.com/ashwanthkumar/slack-go-webhook"
)

var (
	CONFIG_SMTP_HOST string
	CONFIG_SMTP_PORT int

	CONFIG_SLACK_USERNAME string
	CONFIG_SLACK_EMOJI    string
)

func init() {
	CONFIG_SMTP_HOST = utils.GetEnvString("CONFIG_SMTP_HOST", "smtp.gmail.com")
	CONFIG_SMTP_PORT = utils.GetEnvInt("CONFIG_SMTP_PORT", 587)

	CONFIG_SLACK_USERNAME = utils.GetEnvString("CONFIG_SLACK_USERNAME", "Administrator")
	CONFIG_SLACK_EMOJI = utils.GetEnvString("CONFIG_SLACK_EMOJI", ":monkey_face:")
}

func main() {
	http.HandleFunc("/auth", Auth)
	http.HandleFunc("/sendmail", SendMail)
	// http.HandleFunc("/sendslack", SendSlack)

	fmt.Println("server started at port 9000")
	s := &http.Server{
		Addr: ":9000",
		//Handler:        myHandler,
		ReadTimeout:  60 * time.Second,
		WriteTimeout: 60 * time.Second,
		// MaxHeaderBytes: 1 << 20,
	}
	s.ListenAndServe()
}

func Auth(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.Write([]byte("404 Not Found"))
	}
	w.Header().Set("Content-Type", "application/json")
	jsonData, _ := ioutil.ReadAll(r.Body)

	res := models.MessageResponse{
		Rc:      "01",
		Message: "Transaction Failed",
	}

	req := models.MessageAuthRequest{}
	if err := json.Unmarshal(jsonData, &req); err != nil {
		fmt.Println("Failed to decode message request: ", err)
		json.NewEncoder(w).Encode(res)
		return
	}

	db.UpdateToken(req.UserList)

	dataUser, err := db.GetUserData(req.UserList)
	if err != nil {
		fmt.Println("Failed to get data user: ", err)
		json.NewEncoder(w).Encode(res)
		return
	}

	resData := []models.MessageDataAuth{}
	jsonDb, _ := json.Marshal(dataUser)
	json.Unmarshal(jsonDb, &resData)

	res.Rc = "00"
	res.Message = "Successful"
	res.Data = resData

	json.NewEncoder(w).Encode(res)
	return
}

func SendMail(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.Write([]byte("404 Not Found"))
	}

	w.Header().Set("Content-Type", "application/json")
	jsonData, _ := ioutil.ReadAll(r.Body)

	res := models.MessageResponse{
		Rc:      "01",
		Message: "Transaction Failed",
	}

	token := strings.Split(r.Header.Get("Authorization"), " ")
	if len(token) < 2 {
		fmt.Println("Authorization is nil")
		res.Rc = "06"
		res.Message = "Invalid Authorization"
		json.NewEncoder(w).Encode(res)
		return
	}

	dataToken, err := db.GetDataToken(token[1])
	if err != nil {
		fmt.Println("Invalid Authorization: ", err)
		res.Rc = "06"
		res.Message = "Invalid Authorization"
		json.NewEncoder(w).Encode(res)
		return
	}
	if dataToken.ExpiredToken < time.Now().Unix() {
		fmt.Println("Token Expired")
		res.Rc = "07"
		res.Message = "Expired Token"
		json.NewEncoder(w).Encode(res)
		return
	}

	req := models.MessageRequest{}
	if err := json.Unmarshal(jsonData, &req); err != nil {
		fmt.Println("Failed to decode message request: ", err)
		res.Rc = "04"
		res.Message = "Bad Request"
		json.NewEncoder(w).Encode(res)
		return
	}

	errEmailChan := EmailMessage(dataToken, req)
	errSlackChan := SlackMessage(dataToken, req)

	// Email Section
	errEmail := <-errEmailChan
	if errEmail != nil {
		if errEmail.Error() == "04" {
			res.Rc = "04"
			res.Message = "Bad Request"
			json.NewEncoder(w).Encode(res)
			return
		}
		if errEmail.Error() == "02" {
			res.Rc = "02"
			res.Message = "SMTP Error"
			json.NewEncoder(w).Encode(res)
			return
		}
		json.NewEncoder(w).Encode(res)
		return
	}

	// Slack Section
	errSlack := <-errSlackChan
	if errSlack != nil {
		if errSlack.Error() == "04" {
			res.Rc = "04"
			res.Message = "Invalid Channel"
			json.NewEncoder(w).Encode(res)
			return
		}
		if errSlack.Error() == "03" {
			res.Rc = "03"
			res.Message = "Invalid Slack Webhook"
			json.NewEncoder(w).Encode(res)
			return
		}
		json.NewEncoder(w).Encode(res)
		return
	}

	res.Rc = "00"
	res.Message = "Successful"
	json.NewEncoder(w).Encode(res)

	return
}

func EmailMessage(dataToken models.DBMessageDataAuth, req models.MessageRequest) <-chan error {
	errorChan := make(chan error)
	go func() {
		if req.Email == "" {
			fmt.Println("Field `to` should not be empty")
			errorChan <- errors.New("04")
			return
		}

		if req.Subject == "" {
			fmt.Println("Field `subject` should not be empty")
			errorChan <- errors.New("04")
			return
		}

		message := ""
		if req.CompanyName != "" {
			message = message + "Company Name: " + req.CompanyName + "\n"
		}
		if req.Phonenumber != "" {
			message = message + "Phone Number: " + req.Phonenumber + "\n"
		}
		if req.Country != "" {
			message = message + "Country: " + req.Country + "\n"
		}
		message = message + req.Message
		to := strings.Split(strings.TrimSpace(req.Email), ",")
		allRecipients := to
		bodyMessage := "From: " + dataToken.Email + "\n" +
			"To: " + strings.Join(to, ",") + "\n" +
			"Subject: " + req.Subject + "\n\n" + message
		auth := smtp.PlainAuth("", dataToken.Email, dataToken.EmailPassword, CONFIG_SMTP_HOST)
		smtpAddr := fmt.Sprintf("%s:%d", CONFIG_SMTP_HOST, CONFIG_SMTP_PORT)

		fmt.Println("Body Message: ", bodyMessage)
		err := smtp.SendMail(smtpAddr, auth, dataToken.Email, allRecipients, []byte(bodyMessage))
		if err != nil {
			fmt.Println("Get error from smtp server: ", err)
			errorChan <- errors.New("02")
			return
		}

		fmt.Println("Email Sent!")

		errorChan <- nil
		close(errorChan)
		return
	}()
	return errorChan
}

func SlackMessage(dataToken models.DBMessageDataAuth, req models.MessageRequest) <-chan error {
	errorChan := make(chan error)
	go func() {
		message := ""
		if req.CompanyName != "" {
			message = message + "Company Name: " + req.CompanyName + "\n"
		}
		if req.Phonenumber != "" {
			message = message + "Phone Number: " + req.Phonenumber + "\n"
		}
		if req.Country != "" {
			message = message + "Country: " + req.Country + "\n"
		}
		message = message + req.Message

		payload := slack.Payload{
			Text:     message,
			Username: CONFIG_SLACK_USERNAME,
			// Channel:   req.Channel,
			IconEmoji: CONFIG_SLACK_EMOJI,
			// Attachments: []slack.Attachment{attachment1},
		}
		errs := slack.Send(dataToken.SlackHook, "", payload)
		if len(errs) > 0 {
			fmt.Printf("errsor: %s\n", errs[0])
			if strings.Contains(errs[0].Error(), "403") {
				errorChan <- errors.New("03")
				return
			}
			if strings.Contains(errs[0].Error(), "404") {
				errorChan <- errors.New("04")
				return
			}
			errorChan <- errs[0]
			return
		}
		fmt.Println("Slack Sent!")

		errorChan <- nil
		close(errorChan)
		return
	}()
	return errorChan
}
