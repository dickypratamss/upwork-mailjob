#!/usr/bin/env bash

go build

export CONFIG_SMTP_HOST="smtp.gmail.com"
export CONFIG_SMTP_PORT=587

export CONFIG_SLACK_USERNAME="Administrator"
export CONFIG_SLACK_EMOJI=":monkey_face:"

export DB_MYSQL_TYPE_USER="root"
export DB_MYSQL_TYPE_PASS=""
export DB_MYSQL_TYPE_NAME="upwork"
export DB_MYSQL_TYPE_HOST="localhost"
export DB_MYSQL_TYPE_PORT="3306"
export DB_MYSQL_TYPE_SSLMODE="disable"

nohup ./upwork-mailjob > nohup.out 2>&1 &

