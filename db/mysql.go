package db

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"upwork-mailjob/utils"
)

var (
	Dbcon    *gorm.DB
	Errdb    error
	dbuser   string
	dbpass   string
	dbname   string
	dbaddres string
	dbport   string
	dbdebug  bool
	dbtype   string
	sslmode  string
)

func init() {

	fmt.Println("DB MYSQL")

	dbtype = utils.GetEnvString("DB_MYSQL_TYPE", "MYSQL")
	dbuser = utils.GetEnvString("DB_MYSQL_TYPE_USER", "root")
	dbpass = utils.GetEnvString("DB_MYSQL_TYPE_PASS", "")
	dbname = utils.GetEnvString("DB_MYSQL_TYPE_NAME", "upwork")
	dbaddres = utils.GetEnvString("DB_MYSQL_TYPE_HOST", "localhost")
	dbport = utils.GetEnvString("DB_MYSQL_TYPE_PORT", "3306")
	sslmode = utils.GetEnvString("DB_MYSQL_TYPE_SSLMODE", "disable")
	dbdebug = true
	if DbOpen() != nil {
		//panic("DB Can't Open")
		fmt.Println("Can Open db mysql")
	}
	Dbcon = GetDbCon()
	Dbcon = Dbcon.LogMode(true)

}

// DbOpen ..
func DbOpen() error {
	args := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", dbuser, dbpass, dbaddres, dbport, dbname)
	Dbcon, Errdb = gorm.Open("mysql", args)

	if Errdb != nil {
		fmt.Println("open db Err ", Errdb)
		return Errdb
	}

	if errping := Dbcon.DB().Ping(); errping != nil {
		return errping
	}
	return nil
}

// GetDbCon ..
func GetDbCon() *gorm.DB {
	//TODO looping try connection until timeout
	// using channel timeout
	if errping := Dbcon.DB().Ping(); errping != nil {
		fmt.Println("Db Not Connect test Ping :", errping)
		errping = nil
		if errping = DbOpen(); errping != nil {
			fmt.Println("try to connect again but error :", errping)
		}
	}
	Dbcon.LogMode(dbdebug)
	return Dbcon
}
