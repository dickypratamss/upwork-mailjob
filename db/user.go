package db

import (
	"Otherside/upwork-mailjob/models"
	"Otherside/upwork-mailjob/utils"
	"fmt"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

func GetUserData(userList []string) ([]models.DBMessageDataAuth, error) {
	result := []models.DBMessageDataAuth{}
	strUser := strings.Join(userList, ",")
	err := Dbcon.Where("username in (?)", strUser).Find(&result).Error
	if gorm.IsRecordNotFoundError(err) {
		return result, nil
	}
	if err != nil {
		return result, err
	}
	return result, nil
}

func GetDataToken(token string) (models.DBMessageDataAuth, error) {
	result := models.DBMessageDataAuth{}
	err := Dbcon.Where("token = ?", token).Find(&result).Error
	if err != nil {
		return result, err
	}
	return result, nil
}

func UpdateToken(userList []string) error {
	expired := time.Now().AddDate(0, 1, 0).Unix()
	for _, val := range userList {
		token := utils.Rand64String(64)
		err := Dbcon.Exec("UPDATE mail_user SET token = ?, expired_token = ? WHERE username = ?", token, expired, val).Error
		if err != nil {
			fmt.Println("Failed to update data token: ", err)
			return err
		}
	}
	return nil
}
