# Mail Forwarding

This is guide for running this project and how to testing it.

## Config:
``` 
- Clone this repository at your GOPATH directory
- Open the `run.sh` file
- you can fill Config SMTP, your Email, and Slack Webhook
- Don't forget to import `db.sql` to your mysql database 
```

## Run:
```
For running this project just run `./run.sh`. And this project will start in port `9000`
```

## Testing:
```
Just import this postman `https://www.getpostman.com/collections/28b880d2d5dd82470900` for testing
```


Thanks before